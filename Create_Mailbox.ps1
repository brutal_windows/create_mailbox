# Переменная с контейнером, откуда осуществляется выборка пользователей
$Searchbase = ""

# Переменная со статусом пользователя Enabled = True/False
$Status = ""

# Первичный сформированный файл с пользователями
$CSVFileTmp = ""

# Итоговый файл с пользователями
$CSVFile = ""

# База данных для электронной почты пользователей
$Database = "ES1 MDB Ord3"

# Формирование списка активных пользователей, у которых нет почты
Get-ADUser -filter * -SearchBase $Searchbase -Properties * | 
    where -Property Enabled -eq $Status |
	where -Property mail -eq $null |
	Select Name |
	Export-Csv -encoding utf8 $CSVFileTmp -NoTypeInformation
	
# Удаление первой строки и символов '"' из сформированного файла
Get-Content $CSVFileTmp | 
    ForEach-Object {$_ -replace ('"'),' '} | 
	Out-File $CSVFile -Force -Confirm:$false

# Удаление первичного файла с пользователями	
Remove-Item $CSVFileTmp -Force -Confirm:$false

# Создание почтовых ящиков для пользователей из сформированного файла
Import-Csv $CSVFile | 
    ForEach-Object {Enable-Mailbox -Identity $_.Name -Database $Database}
